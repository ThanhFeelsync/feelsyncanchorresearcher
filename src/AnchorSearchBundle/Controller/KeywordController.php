<?php

namespace AnchorSearchBundle\Controller;

use AnchorSearchBundle\Entity\Keyword;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Keyword controller.
 *
 * @Route("/")
 */
class KeywordController extends Controller
{
    /**
     * Lists all keyword entities.
     *
     * @Route("/", name="keyword_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $keywords = $em->getRepository('AnchorSearchBundle:Keyword')->findAll();

        $keyword = new Keyword();
        $form = $this->createForm('AnchorSearchBundle\Form\KeywordType', $keyword);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($keyword);
            $em->flush();

            return $this->redirectToRoute('keyword_index');
        }

        return $this->render('keyword/index.html.twig', array(
            'keywords' => $keywords,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a keyword entity.
     *
     * @Route("/{id}", name="keyword_show")
     * @Method("GET")
     */
    public function showAction(Keyword $keyword)
    {
        return $this->render('keyword/show.html.twig', array(
            'keyword' => $keyword
        ));
    }

}
