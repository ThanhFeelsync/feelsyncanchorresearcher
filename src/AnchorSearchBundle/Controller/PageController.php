<?php

namespace AnchorSearchBundle\Controller;

use AnchorSearchBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Page controller.
 *
 * @Route("page")
 */
class PageController extends Controller
{
    /**
     * Finds and displays a page entity.
     *
     * @Route("/{id}", name="page_show")
     * @Method("GET")
     */
    public function showAction(Page $page)
    {

        return $this->render('page/show.html.twig', array(
            'page' => $page,
        ));
    }
}
