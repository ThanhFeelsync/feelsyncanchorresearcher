<?php
/**
 * Created by PhpStorm.
 * User: DragonAG
 * Date: 10/7/2017
 * Time: 11:57 AM
 */

namespace AnchorSearchBundle\Command;
use AnchorSearchBundle\Utils\GoogleSearchCrawler;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class GrabOneCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:grab-google-anchor:one')
            ->setDescription('Grab one keyword.')
            ->setHelp('This command allows you Grab one Keyword not done');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $keywordRepository  = $doctrine->getRepository('AnchorSearchBundle:Keyword');
        $objKeyword = $keywordRepository->getOneNotDone();
        if(!is_null($objKeyword)) {
            $googleSearchCrawler = new GoogleSearchCrawler();
            $googleSearchCrawler->setDoctrineManager($em);
            $googleSearchCrawler->setOutPut($output);
            $output->writeln([
                'Processing keyword: ' . $objKeyword->getKeyword(),// A line
                '============',// Another line
                '',// Empty line
            ]);
            $googleSearchCrawler->search($objKeyword);

            $output->writeln('Grab successfully!');
        } else {
            $output->writeln('<info>Don\'t have any keyword to do!</info>');
            $output->writeln('<info>foo</info>');
        }
    }
}