<?php
/**
 * Created by PhpStorm.
 * User: DragonAG
 * Date: 10/8/2017
 * Time: 5:05 PM
 */

namespace AnchorSearchBundle\Utils;

use AnchorSearchBundle\Entity\Page;
use AnchorSearchBundle\Entity\Keyword;
use AnchorSearchBundle\Entity\PageDetail;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use GuzzleHttp\Cookie\FileCookieJar;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Psr\Log\LogLevel;

class GoogleSearchCrawler
{

    protected $fetchUrl = "https://www.google.com.vn/search?q=[KEYWORD]&start=[PAGE]";

    protected $keyword;

    /**
     * Current search page
     * @var integer
     */
    protected $currentSearchPage = 0;

    /**
     * Max number of results to retrieve
     * @var integer
     */
    protected $maxResults = 10;

    /**
     * Counts the number of results fetched as not all search pages has 10 results
     * @var integer
     */
    protected $resultsFetched = 0;

    /**
     * Timeout between calls in miliseconds
     * @var integer
     */
    protected $timeout = 2000;

    /**
     * Last results fetched
     * @var Array
     */
    protected $results = [];

    /**
     * Checks if ended results
     * @var boolean
     */
    protected $finished = false;

    /**
     * Domain to end finding values
     * @var string
     */
    protected $domain = null;

    /**
     * Doctrine manager
     * @var
     */
    protected $doctrineManager;

    /**
     * Crawler options
     * @var Array
     */
    protected $crawlerOptions = [];

    /**
     * set logger to the crawler
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * Output commandline
     * @var \Symfony\Component\Console\Output\OutputInterface
     */
    protected $output;

    public function buildUrl(Keyword $keyword, $currentSearchPage)
    {
        return str_replace(
            ['[KEYWORD]', '[PAGE]'],
            [urlencode($keyword->getKeyWord()), $currentSearchPage],
            $this->fetchUrl
        );
    }

    public function search(Keyword $keyword)
    {
        $this->keyword = $keyword;
        switch ($keyword->getStatus()) {
            case 0:

                while ($this->nextSearchPage()) {
                    continue;
                }

                $this->keyword->setStatus(1);
                $this->doctrineManager->persist($this->keyword);
                $this->doctrineManager->flush();
                $this->grabPageDetail();

                break;
            default:
                $this->grabPageDetail();

                break;
        }
    }

    protected function increaseResultsFetched($n)
    {
        $this->resultsFetched += $n;
        if ($this->resultsFetched >= $this->maxResults) {
            $this->finished = true;
        }
    }

    public function hasNextSearchPage()
    {
        return !$this->finished;
    }

    /**
     * Loads the next search page and returns true if there are new results
     * @return boolean
     */
    public function nextSearchPage()
    {
        if ($this->hasNextSearchPage()) {
            $newSearchPage = $this->fetchSearchPage();

            $this->parseSearchPage($newSearchPage);
            if ($this->hasResults()) {
                $this->currentSearchPage++;
                usleep($this->timeout);

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function fetchSearchPage()
    {
        $url = $this->buildUrl($this->keyword, $this->currentSearchPage);
        $code = $this->fetchPageCode($url);
        return mb_convert_encoding($code, 'UTF-8', 'ISO-8859-1');
    }

    public function fetchPageCode($url)
    {
        try {
            $client = new Client();
            $this->writeln('<info>Begin Fetch Search Url: </info>' . $url);
            $response = $client->get($url, $this->crawlerOptions);
        } catch (ClientException $clientEx) {
            $this->writeln('<fg=red>Fetch Url Error: </>' . $clientEx->getResponse()->getStatusCode());
            return $clientEx->getResponse()->getBody(true);
        }
        return $response->getBody(true);
    }

    /**
     * Adds a header value to the requests
     * @param string $key Header key
     * @param string $value Header value
     */
    public function setHeader($key, $value)
    {
        $this->crawlerOptions['headers'][$key] = $value;
    }

    /**
     * Sets a Guzzle request option.
     * See more: http://docs.guzzlephp.org/en/stable/request-options.html
     * @param string $key Guzzle request option key
     * @param string $value Guzzle request option value
     */
    public function setCrawlerOption($key, $value)
    {
        $this->crawlerOptions[$key] = $value;
    }

    /**
     * Gets the Last results fetched.
     *
     * @return Array
     */
    public function getResults()
    {
        return $this->keyword->getPages();
    }

    public function hasResults()
    {
        return $this->keyword->getPages()->count() > 0;
    }

    /**
     * Gets the Counts the number of results fetched as not all search pages has 10 results.
     *
     * @return integer
     */
    public function getResultsFetched()
    {
        return $this->resultsFetched;
    }

    /**
     * Sets the Counts the number of results fetched as not all search pages has 10 results.
     *
     * @param integer $resultsFetched the results fetched
     *
     * @return self
     */
    public function setResultsFetched($resultsFetched)
    {
        $this->resultsFetched = $resultsFetched;

        return $this;
    }

    /**
     * Gets the Max number of results to retrieve.
     *
     * @return integer
     */
    public function getMaxResults()
    {
        return $this->maxResults;
    }

    /**
     * Sets the Max number of results to retrieve.
     *
     * @param integer $maxResults the max results
     *
     * @return self
     */
    public function setMaxResults($maxResults)
    {
        $this->maxResults = $maxResults;

        return $this;
    }

    public function parseSearchPage($content)
    {

        // Get results
        $crawler = new Crawler();
        $crawler->addHtmlContent($content);

        $object = &$this;

        $crawler->filter('#res div.g')->each(
            function (Crawler $match, $i) use ($object) {
                if ($this->finished) {
                    return;
                }

                // Get title and url
                $titleMatch = $match->filter("h3.r > a");
                $url = $this->fixHref($titleMatch->eq(0)->attr("href"));
                $title = $titleMatch->eq(0)->text();

                if (filter_var($url, FILTER_VALIDATE_URL)) {
                    $object->increaseResultsFetched(1);
                    $page = new Page();
                    $page->setKeyword($this->keyword);
                    $page->setRank($this->getResultsFetched());
                    $page->setTitle($title);
                    $page->setUrl($url);

                    // Get description
                    $descriptionMatch = $match->filter(".s .st");
                    if ($descriptionMatch->count() == 1) {
                        $page->setMetaDesc($descriptionMatch->eq(0)->text());
                    }

                    // Add result
                    $this->keyword->addPage($page);

                    $this->writeln("<info>Get Title $this->resultsFetched:</info> " . $title);
                    $this->writeln("<info>Get Url $this->resultsFetched:</info> " . $url);
                }
            }
        );
    }

    public function setDoctrineManager($doctrineManager)
    {
        $this->doctrineManager = $doctrineManager;

        return $this;
    }


    public function fixHref($strHref)
    {
        $strQuery = parse_url($strHref, PHP_URL_QUERY);
        parse_str($strQuery, $queryParam);

        return isset($queryParam['q']) ? $queryParam['q'] : '';
    }

    private function grabPageDetail()
    {
        $this->keyword->setStatus(2);
        $this->doctrineManager->persist($this->keyword);
        $this->doctrineManager->flush();
        foreach ($this->keyword->getPages() as $page) {
            $this->writeln("<info>Fetching Page url:</info> " . $page->getUrl());
            $code = $this->fetchPageCode($page->getUrl());
            $pageCode = mb_convert_encoding($code, "UTF-8");
            $this->parsePage($pageCode, $page);
            usleep($this->timeout);
        }
        $this->keyword->setStatus(3);
        $this->doctrineManager->persist($this->keyword);
        $this->doctrineManager->flush();
    }

    /**
     * Parse html content page and update to database
     *
     * @param string $content
     * @param AnchorSearchBundle :Page $page
     */
    public function parsePage($content, $page)
    {
        $base_url_host = parse_url($page->getUrl(), PHP_URL_HOST);

        // Get results
        $crawlerPage = new Crawler();
        $crawlerPage->addHtmlContent($content);

        $crawlerPage->filter('a')->each(
            function (Crawler $node, $i) use ($page, $base_url_host) {
                $url = $node->attr('href');
                $url_host = parse_url($url, PHP_URL_HOST);
                $anchorText = $node->text();

                $checkNodeImage = $node;
                if (!empty($url_host) && $url_host != $base_url_host) {
                    $pageDetail = new PageDetail();
                    $pageDetail->setAnchorText($anchorText);
                    $pageDetail->setAnchorUrl($url);
                    if ($checkNodeImage->filter('img')->count()) {
                        $nodeImg = $checkNodeImage->filter('img')->getNode(0);
                        $pageDetail->setAnchorType('Image');
                        $pageDetail->setAnchorText($nodeImg->getAttribute('alt'));
                        $this->writeln("<info>Get Page Image:</info> " . $url);
                    } else {
                        $pageDetail->setAnchorType('Text');
                        $this->writeln("<info>Get Page Text:</info> " . $url);
                    }
                    $pageDetail->setPage($page);
                    $page->addPageDetail($pageDetail);
                }
            }
        );
        $page->setStatus(1);
        $this->doctrineManager->persist($page);
        $this->doctrineManager->flush();
    }

    /**
     * logging activity of the crawler in case logger is associated
     * @param string $level
     * @param string $message
     * @param array $context
     */
    protected function log($level, $message, array $context = array())
    {
        if (isset($this->logger) === true) {
            $this->logger->log($level, $message, $context);
        }
    }

    /**
     * set logger to the crawler
     * @param $logger \Psr\Log\LoggerInterface
     * @return GoogleSearchCrawler
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
        return $this;
    }

    protected function writeln($message)
    {
        if (isset($this->output) === true) {
            $this->output->writeln($message);
        }
    }

    /**
     * set logger to the crawler
     * @param $logger \Psr\Log\LoggerInterface
     * @return GoogleSearchCrawler
     */
    public function setOutPut($output)
    {
        $this->output = $output;
        return $this;
    }

}