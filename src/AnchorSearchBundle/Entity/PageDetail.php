<?php

namespace AnchorSearchBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PageDetail
 *
 * @ORM\Table(name="page_detail")
 * @ORM\Entity(repositoryClass="AnchorSearchBundle\Repository\PageDetailRepository")
 */
class PageDetail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="anchor_text", type="string", length=100)
     */
    private $anchorText;

    /**
     * @var string
     *
     * @ORM\Column(name="anchor_type", type="string", length=5)
     */
    private $anchorType;

    /**
 * @var string
 *
 * @ORM\Column(name="anchor_url", type="string", length=100)
 */
    private $anchorUrl;

    /**
     * Many PageDetail have one Page.
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="pageDetails")
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id")
     */
    private $page;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set anchorText
     *
     * @param string $anchorText
     *
     * @return PageDetail
     */
    public function setAnchorText($anchorText)
    {
        $this->anchorText = $anchorText;

        return $this;
    }

    /**
     * Get anchorText
     *
     * @return string
     */
    public function getAnchorText()
    {
        return $this->anchorText;
    }

    /**
     * Set anchorType
     *
     * @param string $anchorType
     *
     * @return PageDetail
     */
    public function setAnchorType($anchorType)
    {
        $this->anchorType = $anchorType;

        return $this;
    }

    /**
     * Get anchorType
     *
     * @return string
     */
    public function getAnchorType()
    {
        return $this->anchorType;
    }

    /**
     * Set anchorUrl
     *
     * @param string $anchorUrl
     *
     * @return PageDetail
     */
    public function setAnchorUrl($anchorUrl)
    {
        $this->anchorUrl = $anchorUrl;

        return $this;
    }

    /**
     * Get anchorUrl
     *
     * @return string
     */
    public function getAnchorUrl()
    {
        return $this->anchorUrl;
    }

    /**
     * Set page
     *
     * @param \AnchorSearchBundle\Entity\Page $page
     *
     * @return PageDetail
     */
    public function setPage(\AnchorSearchBundle\Entity\Page $page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return \AnchorSearchBundle\Entity\Page
     */
    public function getPage()
    {
        return $this->page;
    }
}
