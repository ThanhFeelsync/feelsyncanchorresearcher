<?php

namespace AnchorSearchBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Page
 *
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="AnchorSearchBundle\Repository\PageRepository")
 */
class Page
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="rank", type="smallint")
     */
    private $rank;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_desc", type="string", length=150, nullable=true)
     */
    private $metaDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=150)
     */
    private $url;

    /**
     * Many Page have one Keyword.
     * @ORM\ManyToOne(targetEntity="Keyword", inversedBy="pages")
     * @ORM\JoinColumn(name="keyword_id", referencedColumnName="id")
     */
    private $keyword;

    /**
     * @ORM\OneToMany(targetEntity="PageDetail", mappedBy="page", cascade={"remove", "persist"})
     */
    private $pageDetails;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status = 0;

    public function __toString()
    {
        return $this->getTitle();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     *
     * @return Page
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return int
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Page
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set keyword
     *
     * @param \AnchorSearchBundle\Entity\Keyword $keyword
     *
     * @return Page
     */
    public function setKeyword(\AnchorSearchBundle\Entity\Keyword $keyword = null)
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * Get keyword
     *
     * @return \AnchorSearchBundle\Entity\Keyword
     */
    public function getKeyword()
    {
        return $this->keyword;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pageDetails = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add pageDetail
     *
     * @param \AnchorSearchBundle\Entity\PageDetail $pageDetail
     *
     * @return Page
     */
    public function addPageDetail(\AnchorSearchBundle\Entity\PageDetail $pageDetail)
    {
        $this->pageDetails[] = $pageDetail;

        return $this;
    }

    /**
     * Remove pageDetail
     *
     * @param \AnchorSearchBundle\Entity\PageDetail $pageDetail
     */
    public function removePageDetail(\AnchorSearchBundle\Entity\PageDetail $pageDetail)
    {
        $this->pageDetails->removeElement($pageDetail);
    }

    /**
     * Get pageDetails
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPageDetails()
    {
        return $this->pageDetails;
    }

    /**
     * Set metaDesc
     *
     * @param string $metaDesc
     *
     * @return Page
     */
    public function setMetaDesc($metaDesc)
    {
        $this->metaDesc = $metaDesc;

        return $this;
    }

    /**
     * Get metaDesc
     *
     * @return string
     */
    public function getMetaDesc()
    {
        return $this->metaDesc;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Page
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
}
