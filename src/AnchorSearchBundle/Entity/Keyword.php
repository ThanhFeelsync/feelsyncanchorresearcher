<?php

namespace AnchorSearchBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Keyword
 *
 * @ORM\Table(name="keyword")
 * @ORM\Entity(repositoryClass="AnchorSearchBundle\Repository\KeywordRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Keyword
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="key_word", type="string", length=100)
     */
    private $keyWord;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="is_processing", type="smallint")
     */
    private $isProcessing = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="input_date", type="datetime")
     */
    private $inputDate;

    /**
     * @ORM\OneToMany(targetEntity="Page", mappedBy="keyword", cascade={"remove", "persist"})
     */
    private $pages;

    /**
     * Get status text display to frontend
     * @return string
     */
    public function getStatusText()
    {
        switch($this->status){
            case 0:
                return 'Todo';
            case 1:
                return 'In Progress[Anchor generate]';
            case 2:
                return 'In Progress[Url List generate]';
            case 3:
                return 'Done';
        }
    }

    public function isTodo()
    {
        return ($this->status == 0);
    }

    public function isProcessAnchor()
    {
        return ($this->status == 1);
    }

    public function isProcessUrlGen()
    {
        return ($this->status == 3);
    }

    public function setStatusAnchorGenerate()
    {
        return $this->setStatus(1);
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set keyWord
     *
     * @param string $keyWord
     *
     * @return Keyword
     */
    public function setKeyWord($keyWord)
    {
        $this->keyWord = $keyWord;

        return $this;
    }

    /**
     * Get keyWord
     *
     * @return string
     */
    public function getKeyWord()
    {
        return $this->keyWord;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Keyword
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @ORM\PrePersist
     *
     * Set inputDate
     *
     * @param \DateTime $inputDate
     *
     * @return Keyword
     */
    public function setInputDate($inputDate)
    {
        $this->inputDate = new \DateTime();

        return $this;
    }

    /**
     * Get inputDate
     *
     * @return \DateTime
     */
    public function getInputDate()
    {
        return $this->inputDate;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add page
     *
     * @param \AnchorSearchBundle\Entity\Page $page
     *
     * @return Keyword
     */
    public function addPage(\AnchorSearchBundle\Entity\Page $page)
    {
        $this->pages[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \AnchorSearchBundle\Entity\Page $page
     */
    public function removePage(\AnchorSearchBundle\Entity\Page $page)
    {
        $this->pages->removeElement($page);
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * Set isProcessing
     *
     * @param integer $isProcessing
     *
     * @return Keyword
     */
    public function setIsProcessing($isProcessing)
    {
        $this->isProcessing = $isProcessing;

        return $this;
    }

    /**
     * Get isProcessing
     *
     * @return integer
     */
    public function getIsProcessing()
    {
        return $this->isProcessing;
    }
}
